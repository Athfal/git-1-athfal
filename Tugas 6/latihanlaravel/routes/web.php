<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexConstroller;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexConstroller::class, 'index']);
Route::get('/biodata', [AuthController::class, 'bio']);
Route::post('/Sign Up', [AuthController::class, 'kirim']);

Route::get('/data-table', [IndexConstroller::class, 'table']);

//CRUD Cast
//Create
//route yang mengarah ke form tambah cast
route::get('/cast/create', [CastController::class, 'create']);
//route tambah data ke database
route::post('/cast', [CastController::class, 'store']);

route::get('/cast', [CastController::class, 'index']);

route::get('/cast/{id}', [CastController::class, 'show']);

route::get('/cast/{id}/edit', [CastController::class, 'edit']);
route::put('/cast/{id}', [CastController::class, 'update']);
route::delete('/cast/{id}', [CastController::class, 'destroy']);
