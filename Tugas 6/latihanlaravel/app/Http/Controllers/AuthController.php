<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('halaman.biodata');
    }
    public function kirim(Request $request)
    {
        $FirstName = $request['depan'];
        $LastName = $request['belakang'];

        return view('halaman.home', ['FirstName' => $FirstName, 'LastName' => $LastName] );
    }
}
