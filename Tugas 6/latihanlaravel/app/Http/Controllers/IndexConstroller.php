<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexConstroller extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function table(){
        return view('halaman.data-table');
    }
}
