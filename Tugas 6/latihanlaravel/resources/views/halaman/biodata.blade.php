@extends('layout.master')

@section('title')
Halaman Biodata
@endsection

@section('content')
    <h1> Buat Account Baru </h1>
    <form action="/Sign Up" method="post">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="depan"><br><br>
        <Label>Last Name:</Label><br><br>
        <input type="text" name="belakang"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gn" value="1">Male<br>
        <input type="radio" name="gn" value="2">Female<br>
        <input type="radio" name="gn" value="3">Other<br><br>
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="1">Indonesia</option>
            <option value="2">WNA</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="Checkbox" name="lang" value="1">Bahasa Indonesia <br>
        <input type="Checkbox" name="lang" value="2">English<br>
        <input type="Checkbox" name="lang" value="3">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
    @endsection