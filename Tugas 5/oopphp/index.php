<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");

echo "Nama Hewan : " . $sheep->name . "<br>";
echo "Jumlah  Kaki : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br> <br>";

$frog = new Frog("Buduk");

echo "Nama Hewan : " . $frog->name . "<br>";
echo "Jumlah  Kaki : " . $frog->legs . "<br>";
echo "Cold Blooded : " . $frog->cold_blooded . "<br>";
echo "Jump : " . $frog->jump . "<br> <br>";

$sungokong = new Ape("Kera Sakti");

echo "Nama Hewan : " . $sungokong->name . "<br>";
echo "Jumlah  Kaki : " . $sungokong->legs . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell . "<br> <br>";


?>
